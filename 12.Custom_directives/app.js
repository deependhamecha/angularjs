var myApp = angular.module('myApp',['ngRoute']);

myApp.config(function($routeProvider){
    
    $routeProvider
    .when('/',{
        templateUrl : 'main.html',
        controller : 'mainController'
    })
    .when('/second',{
        templateUrl : 'second.html',
        controller : 'secondController'
    })
});

myApp.controller('mainController',function($scope) {

});

myApp.controller('secondController',function($scope) {
    
    
});

myApp.directive("searchResult",function() { // name searchResult will be converted to search-result
    
    
    return { 
        restrict : 'AEMC', // it tells that accecpt this directive as an A=attribute AND E= Element AND C=Class AND M=Comment
        //actual directive ...THIS WILL INSERT INTO <search-result> directive
//        template: '<a href="#" class="list-group-item"><h4 class="list-group-item-heading">Deepen Dhamecha</h4><p class="list-group-item-text">IT Engineer</p></a>',
        // Now, this html string can be pretty ugly at times so save it to another file & give path
        templateUrl : 'directives/search-result.html', //u can name directives folder as u want, nothing personal
        
        replace: true   // this will fully replace check inspect, It will replace search-result with html template, if false it will have <search-result/>
        
        // Note : if replace : false then restrict:'M' wont work, as it is a simple comment
    }
});